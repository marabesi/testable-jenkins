[![Codacy Badge](https://api.codacy.com/project/badge/Grade/b22163ba0ed9450d89d6e7b2ef71b886)](https://app.codacy.com/app/matheus-marabesi/testable?utm_source=github.com&utm_medium=referral&utm_content=marabesi/testable&utm_campaign=Badge_Grade_Settings)
[![Build Status](https://travis-ci.org/marabesi/testable.svg?branch=master)](https://travis-ci.org/marabesi/testable)
[![Coverage Status](https://coveralls.io/repos/github/marabesi/testable/badge.svg?branch=master)](https://coveralls.io/github/marabesi/testable?branch=master)

## Testable - gamified tool to improve unit testing teaching

![Testable palette](concept.jpg "Testable palette")
