import React from 'react';
import SurveyModal from '../../components/survey/Survey';

const SurveyPage = () => <SurveyModal skip={true} />;

export default SurveyPage;