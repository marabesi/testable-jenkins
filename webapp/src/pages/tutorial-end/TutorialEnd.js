import content from './tutorial-end-content.json';
import WrappedSceneContentManager from '../../components/scene-manager/SceneContentManager';

export default WrappedSceneContentManager(
  'tutorial_end',
  content,
  'unit-testing-intro'
);
