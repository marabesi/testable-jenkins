import content from './unit-testing-end-content.json';
import WrappedSceneContentManager from '../../components/scene-manager/SceneContentManager';

export default WrappedSceneContentManager(
  'unit-testing-end',
  content,
  'rocket-01'
);
